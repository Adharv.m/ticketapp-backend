class Model{
    constructor(){
        this.seats=[false,false,true,false,false,false,false,false,false,false]
    }
    getData(){
        return this.seats
    }
    getSeat(i){
        return this.seats[i];
    }
    bookSeat(i){
        if(this.seats[i]===true){
            this.seats[i] =false;
        }else{
            this.seats[i] =true;
        }
    }
    
}
exports.Model= Model