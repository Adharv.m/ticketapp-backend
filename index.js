const express = require("express");
var cors = require('cors')
const { Model } = require('./model/model') 
const {BookingModel} = require('./bookingModel')
const booking = new BookingModel()
const model = new Model();
const app = express();
app.use(cors())
const port = 3000

app.use(express.json());

app.get('/', (req, res) => {
    console.log('inside /', model.getData())
    res.send('hello world')
})

app.get('/seats',(req, res) => {
    res.json(model.getData())
})
app.get('/booking',(req,res)=>{
    res.json(booking.getBooking())
})
app.post('/booking',(req,res)=>{
    console.log('inside /booking', req.body)
    let isValid=true
    let errorMessage = '';
    if(req.body.name===""||req.body.name==null){
        isValid=false
        errorMessage = 'name is required'
    }
    if(req.body.phone?.length<10||req.body.phone==null){
        isValid=false
        errorMessage ='number must be at least 10 digits'
    }
    if(req.body.email===""||req.body.email==null){
        isValid=false
         errorMessage ='email must be required'
    }
    if(req.body.seatsSelected?.length===0||req.body.seatsSelected==null){
        isValid=false
         errorMessage ='no seat selected'
    }
    if(req.body.seatsSelected){
        for(i=0;i<req.body.seatsSelected.length;i++){
            let selectedSeat = req.body.seatsSelected[i]
            if(model.getSeat(selectedSeat)){
                isValid=false
                errorMessage='seats selected are already booked'
            }
        }
    }
    if(isValid){

        const result= booking.setBooking(req.body)
        for(i=0;i<req.body.seatsSelected.length;i++){
            model.bookSeat(req.body.seatsSelected[i])

        
       
        }
        res.status(201).json(result)
    }else{
        res.status(400).send(errorMessage)
    }
})

app.listen(port, () =>{
    console.log(`Example app listening on port ${port}`)

})